#!/usr/bin/env python3
from collections import defaultdict
import re

# in_file = 'example'
in_file = 'input'

r = r'(.*)-(.*)'
paths_lr = defaultdict(list)
paths_rl = defaultdict(list)
for a,b in re.findall(r, open(in_file).read()):
	paths_lr[a].append(b)
	paths_rl[b].append(a)

def solve(part2=False):
	open_list = []
	open_list.append(["start"])
	closed_list = []
	while len(open_list):
		print(len(open_list),len(closed_list))
		path = open_list[0]
		if 'end' not in path:
			for way in paths_lr[path[-1]]+paths_rl[path[-1]]:
				if path+[way] not in closed_list:
					if len(way) <= 2 and way.isupper(): # large caves
						open_list.append(path+[way])
					elif len(way) <= 2 and way.islower() and way not in path: # small caves, visited once
						open_list.append(path+[way])
					elif part2 and len(way) <= 2 and way.islower() and path.count(way) <= 1: # small caves, one is visited twice
						part2_possible = True
						for w in path:
							if w.islower() and path.count(w) == 2:
								part2_possible = False
						if part2_possible:
							open_list.append(path+[way])
					elif way not in path: # start and end
						open_list.append(path+[way])
		open_list.remove(path)
		if 'end' in path:
			closed_list.append(path)
	print(len(closed_list))

solve()
solve(True)
