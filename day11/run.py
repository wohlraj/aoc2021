#!/usr/bin/env python3
import itertools

# in_file = 'example'
in_file = 'input'

g = {}
with open(in_file) as f:
	lines = f.read().splitlines()
	I=1
	for l in lines:
		J=1j
		for c in l:
			g[I+J] = int(c)
			J+=1j
		I+=1
	s = I
	dirs = [(-1-1j),(-1),(-1+1j),(1-1j),(1),(1+1j),(-1j),(1j)]
	counter = 0

def flash(pos, flashed):
	global counter
	flashed.add(pos)
	counter += 1
	i,j = pos.real, pos.imag
	for d in dirs:
		if pos+d in g.keys():
			g[pos+d] += 1
			if g[pos+d] > 9 and not pos+d in flashed:
				flash(pos+d, flashed)
	return flashed

def step():
	flashed = set()
	# step 1
	for i,j in itertools.product(range(1,s),range(1,s)):
		g[i+j*1j] += 1
	# step 2
	for i,j in itertools.product(range(1,s),range(1,s)):
		pos = i+j*1j
		if g[pos] > 9 and not pos in flashed:
			flashed = flash(pos, flashed)
	# step 3
	while (val:=sorted(g.values(), reverse=True)[0]) > 9:
		idx = list(g.values()).index(val)
		g[list(g.keys())[idx]] = 0

# part 1
n = 100
i = 0
while i < n:
	step()
	i += 1
print(counter)

# part 2
while set(g.values()) != {0}:
	step()
	i += 1
print(i)
