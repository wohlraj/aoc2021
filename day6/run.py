#!/usr/bin/env python3
with open('input') as f:
	l = [int(x) for x in f.read().splitlines()[0].split(',')]
	# packing input as (char, nbr_char)
	t, ll = (char:=l[0], 1), []
	for i in range(1,len(l)):
		if l[i] == char:
			t = (t[0], t[1]+1)
		else:
			ll.append(t)
			t = (char:=l[i], 1)
			i += 1
	ll.append(t)

def compress(l):
	if len(l) <= 1:
		return l
	elif l[0][0] == l[1][0]:
		return [(l[0][0],l[0][1]+l[1][1])]+compress(l[2:])
	return l[0:1]+compress(l[1:])

def solve(n,l):
	if n == 0:
		res = 0
		for _, r in l:
			res += r
		return res
	nl, nl_end = [], []
	for t1, t2 in l:
		if t1 == 0:
			nl.append((6, t2))
			nl_end.append((8, t2))
		else:
			nl.append((t1-1, t2))
	return solve(n-1,compress(nl+nl_end))

print(solve(80,ll))
print(solve(256,ll))
