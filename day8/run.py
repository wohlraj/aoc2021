#!/usr/bin/env python3

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
	data = []
	data.append([l.replace(' |','').split(' ') for l in f.read().splitlines()])
	data = data[0]
# right part starts at [10:]

globalres_part1 = ""
globalres_part2 = 0
for d in data:
	l,r = [d[:10],d[10:]]
	lens = [len(x) for x in l]

	# a is found using 7-1
	na = str(list(set(l[lens.index(3)])-set(l[lens.index(2)]))[0])
	# b is present 6 times
	# c is present 8 times, but is not a
	# e is present 4 times
	# f is present 9 times
	for x in list("abcdefgh"):
		if ''.join(l[:10]).count(x) == 4:
			ne = x
		elif ''.join(l[:10]).count(x) == 6:
			nb = x
		elif ''.join(l[:10]).count(x) == 8 and x != na:
			nc = x
		elif ''.join(l[:10]).count(x) == 9:
			nf = x
	# c is 7-a-f
	nc = str(list(set(l[lens.index(3)])-{na,nf})[0])
	# d is 4 - b - c - f
	nd = str(list(set(l[lens.index(4)])-{nb,nc,nf})[0])
	# g is what's left
	ng = str(list(set(list("abcdefg"))-{na,nb,nc,nd,ne,nf})[0])

	res = ""
	for el in r:
		nel = ""
		subres = ""
		for letter in list(el):
			if letter == na:
				nel += 'a'
			elif letter == nb:
				nel += 'b'
			elif letter == nc:
				nel += 'c'
			elif letter == nd:
				nel += 'd'
			elif letter == ne:
				nel += 'e'
			elif letter == nf:
				nel += 'f'
			elif letter == ng:
				nel += 'g'
		if set(nel)==set(list("abcefg")):
			subres += '0'
		elif set(nel)==set(list("cf")):
			subres += '1'
		elif set(nel)==set(list("acdeg")):
			subres += '2'
		elif set(nel)==set(list("acdfg")):
			subres += '3'
		elif set(nel)==set(list("bcdf")):
			subres += '4'
		elif set(nel)==set(list("abdfg")):
			subres += '5'
		elif set(nel)==set(list("abdefg")):
			subres += '6'
		elif set(nel)==set(list("acf")):
			subres += '7'
		elif set(nel)==set(list("abcdefg")):
			subres += '8'
		elif set(nel)==set(list("abcdfg")):
			subres += '9'
		res += subres
	globalres_part1 += res
	globalres_part2 += int(res)
print(globalres_part1.count('1')+globalres_part1.count('4')+globalres_part1.count('7')+globalres_part1.count('8'))
print(globalres_part2)

# 7 segments: 8
# 6 segments: 0, 6, 9
# 5 segments: 2, 3, 5
# 4 segments: 4
# 3 segments: 7
# 2 segments: 1

# f is present 9 times
# a and c are present 8 times
# d and g are present 7 times
# b is present 6 times
# e is present 4 times
