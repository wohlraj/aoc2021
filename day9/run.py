#!/usr/bin/env python3
from collections import defaultdict
import itertools
import math

# in_file = 'example'
in_file = 'input'

d = defaultdict(lambda:1000)
with open(in_file) as f:
	i = 0
	for l in f.read().splitlines():
		j = 0
		for c in list(l):
			d[(i,j)] = int(c)
			j += 1
		i += 1
w, h = i, j
dirs = [(0,1),(0,-1),(1,0),(-1,0)]

# part 1
lowpoints = []
for i,j in itertools.product(range(w),range(h)):
	r = [d[(i,j)] < d[tuple(map(sum, zip((i,j), dd)))] for dd in dirs]
	if all(r):
		lowpoints.append((i,j))
risk_levels = 0
for p in lowpoints:
	risk_levels += d[p]+1
print(risk_levels)

# part 2
def floodfill(p):
	size = 0
	if d[p] < 9:
		d[p] = -1
		size = 1
	for dd in dirs:
		nij = tuple(map(sum, zip(p, dd)))
		if -1 < d[nij] < 9:
			size += floodfill(nij)
	return size
sizes = []
for p in lowpoints:
	sizes.append(floodfill(p))
print(math.prod(sorted(sizes)[-3:]))
