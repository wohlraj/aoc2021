#!/usr/bin/python3

file = open("input", "r")
x, z = 0, 0
for line in file:
	action, val = line.strip().split(' ')
	if action == "forward":
		x += int(val)
	elif action == "down":
		z += int(val)
	elif action == "up":
		z -= int(val)
print(z*x)

file = open("input", "r")
x, z, aim = 0, 0, 0
for line in file:
	action, val = line.strip().split(' ')
	if action == "forward":
		x += int(val)
		z += aim*int(val)
	elif action == "down":
		aim += int(val)
	elif action == "up":
		aim -= int(val)
print(z*x)
