#!/usr/bin/env python3
from statistics import median

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
    l = [int(x) for x in f.read().splitlines()[0].split(',')]

# part 1
med = int(median(l))
res = 0
for ll in l:
	res += abs(med-ll)
print(res)

# part 2
fuels = []
for i in range(1,max(l)): # loop over positions
	fuel = 0
	for ll in l:
		dist = abs(ll-i)
		fuel += dist*(dist+1)//2
	fuels.append([i,fuel])
print(sorted(fuels,key=lambda a:(a[1]))[0][1])
