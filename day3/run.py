#!/usr/bin/python3

# part 1
with open('input') as f:
	cols = None
	lines = f.read().splitlines()
	for line in lines:
		if cols is None:
			cols = [[] for _ in range(0, len(line)) ]
		i = 0
		for char in line:
			cols[i].append(char)
			i += 1

gamma, epsilon, save = '', '', ''
for c in cols:
	if c.count('0') > c.count('1'):
		gamma += '0'
		epsilon += '1'
	elif c.count('0') < c.count('1'):
		gamma += '1'
		epsilon += '0'
	else:
		print("!!!")
print(int(gamma,2)*int(epsilon,2))

# part 2

def part2(x):
	i = 0
	with open('input') as f:
		lines = f.read().splitlines()
	while len(lines) != 1:
		r = 0
		for l in lines:
			if l[i] == '0':
				r -= 1
			if l[i] == '1':
				r += 1
		new_lines = []
		if not(x):
			r *= -1
		for l in lines:
			if x and (int(l[i]) == (r >= 0)):
				new_lines.append(l)
			if not(x) and (int(l[i]) == (r > 0)):
				new_lines.append(l)
		lines = new_lines
		i += 1
	return(int(lines[0],2))
print(part2(1)*part2(0))
