#!/bin/bash

if ! echo "$1" | grep -qE "^([1-9]|1[0-9]|2[0-5])$"; then
	echo "bad input"
	exit 1
fi

if [ -d "day$1" ]; then
	echo "dir exists"
else
	mkdir "day$1"
fi

if [ -d "day$1/run.py" ]; then
	echo "run.py exists"
else
	cp "tools/template.py" "day$1/run.py"
	touch "day$1/example" "day$1/input"
fi

if $(which subl); then
	subl "day$1/"*
fi
