#!/usr/bin/env python3
from collections import defaultdict
import itertools
import numpy as np

in_file = 'example'
# in_file = 'input'

# # read input with re
# import re
# r = r'(\d*),(\d*) -> (\d*),(\d*)'
# for x1,y1,x2,y2 in re.findall(r, open(in_file).read()):

# # read input with a loop
# with open(in_file) as f:
# 	lines = f.read().splitlines()
# 	for l in lines:
# 		x,y,z = l.split(',')
