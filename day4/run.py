#!/usr/bin/python3

# part 1
boards = []
board_size = 5
with open('input') as f:
	lines = f.read().splitlines()
	board = []
	for line in lines:
		if ',' in line:
			draw = line.split(',')
		elif len(board)==board_size**2:
			boards.append(board)
			board = []
		else:
			board += [el for el in line.split(' ') if el != '']
	boards.append(board) # last board

winners = []
for d in draw:
	i = 0
	for b in boards:
		# marking draw
		if d in b:
			b = [el if el != d else 'X' for el in b]
			boards[i] = b
		# looking for a winner
		for k in range(board_size):
			if b[k*5:k*5+board_size] == ['X' for _ in range(board_size)] or [b[k],b[k+5],b[k+2*5],b[k+3*5],b[k+4*5]] == ['X' for _ in range(board_size)]:
				score = int(d) * sum([int(el) for el in b if el != 'X'])
				if not i in winners:
					if len(winners) == 0:
						print("part 1 :",score)
					winners.append(i)
					if len(winners) == len(boards):
						score = int(d) * sum([int(el) for el in b if el != 'X'])
						print("part 2 :",score)
		i += 1
