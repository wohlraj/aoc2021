#!/usr/bin/python3

file = open("input", "r")
val, new_val, count = 0, 0, 0
for line in file:
	new_val = int(line.strip())
	if new_val > val:
		count += 1
	val = new_val
print(count-1)

file = open("input", "r")
i, l, val, new_val, count = 0, [], 0, 0, 0
for line in file:
	l.append(int(line.strip()))
	if i > 1:
		new_val = sum(l[i-2:i+1])
		if new_val > val:
			count += 1
		val = new_val
	i += 1
print(count-1)
