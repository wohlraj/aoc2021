#!/usr/bin/env python3
import re
def solve(part2=False):
	r = r'(\d*),(\d*) -> (\d*),(\d*)'
	n = 1000
	grid = [[0 for _ in range(n)] for _ in range(n)]
	for x1,y1,x2,y2 in re.findall(r, open('input').read()):
		x1,y1,x2,y2 = int(x1),int(y1),int(x2),int(y2)
		if x1 == x2:
			y1,y2 = sorted([y1,y2])
			for y in range(y2-y1+1):
				grid[x1][y1+y] += 1
		elif y1 == y2:
			x1,x2 = sorted([x1,x2])
			for x in range(x2-x1+1):
				grid[x1+x][y1] += 1
		elif part2:
			if x2>=x1 and y2>=y1:
				for x in range(x2-x1+1):
					grid[x1+x][y1+x] += 1
			elif x2>=x1 and y2<y1:
				for x in range(x2-x1+1):
					grid[x1+x][y1-x] += 1
			elif x2<x1 and y2>=y1:
				for x in range(x1-x2+1):
					grid[x2+x][y2-x] += 1
			elif x2<x1 and y2<y1:
				for x in range(x1-x2+1):
					grid[x1-x][y1-x] += 1

	res = 0
	for x in range(n):
		for y in range(n):
			if grid[x][y] >= 2:
				res += 1
	print(res)

solve(part2=False)
solve(part2=True)
